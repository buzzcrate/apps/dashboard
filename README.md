[[_TOC_]]
# Dashboard

Simple application to show how to use a go template and web server. Additionally, a small websocket app to connect to [BuzzChat](https://gitlab.com/apps/buzzchat) to control the gauge.

## Template Base

Simple template dashboard webserver writtin Go. 

## Build

Use build script. 

## Launch Local

Simple to launch. `./buzzdash 8080` to start and set port. 

Notice, Apps tab has a chart that is interactive.

### Changing Websocket URL and Port

To change the URL port and Websocket, the environment variables must be set for:
* `SERVER_URL`
* `SERVER_PORT`

The connection string must be for the host [BuzzChat](https://gitlab.com/buzzcrate/apps/buzzchat) instance that the app section wants to connect too.

These enviornment variables are configured in Kubernetes via a ConfigMap.

## Kubernetes

To install on Kubernetes, the installation has been made simple.

### Kubernetes Manifest (simple)

In the `k3s/` folder in this repository. Simply modify and then deploy the deployment with `kubectl apply -f k3s-deployment.yaml`.

### Kustomize

Since templating helps, copy the folder `overlays/original` with whatever you want. Use [Kustomize](https://kustomize.io/) to modify the originl deployment. 

To use, easiest way is after the files are modified, use `kubectl apply -k overlays/original` or whatever the folder path happens to be. Feel free to edit as howevever you want. Please note Kustomize is a more approachable simpler way to deploy templated installs compared to [Helm](https://helm.sh/). 

The original Kustomize template is for only modifying the ConfigMap [mentioned above](#changing-websocket-url-and-port).
